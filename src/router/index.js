import HomePage from '../components/home.vue'
import AboutPage from '../components/about.vue'




import ActivitiesViewPage from '../components/activities'
import AttachmentsViewPage from '../components/attachments'
import DynamicRoutePage from '../components/dynamic-route.vue'
import NotFoundPage from '../components/not-found.vue'
import PanelLeftPage from '../components/panel-left.vue'
import PanelRightPage from '../components/panel-right.vue'

// my flow
import TaskPage from    '../components/personalwork/task.vue'
import MyProcessPage from '../components/personalwork/myProcess.vue'
import MyInvolvedProcessPage from '../components/personalwork/myInvolvedProcess.vue'

import AdmitRedHead     from  '../components/personalwork/admit/admitRedHead.vue'
import AdmitEvection    from '../components/personalwork/admit/admitEvection'
import AdmitTeachingCar from '../components/personalwork/admit/admitTeachingCar'
import AdmitCar         from '../components/personalwork/admit/admitCar'
import AdmitRecruit     from '../components/personalwork/admit/admitRecruit'
import AdmitSeal        from '../components/personalwork/admit/admitSeal'
import AdmitSig         from '../components/personalwork/admitSignature'

// workflow
import AdditionListPage from '../components/workflow/additionList.vue'

import ApplyRedHeadPage from        '../components/workflow/applyRedHead.vue'
import ApplyRedHeadTablePage from   '../components/workflow/applyRedHeadTable.vue'
import RedHeadApplyViewPage from    '../components/redheadapply.vue'

import ApplySealPage from           '../components/workflow/applySeal.vue'
import ApplySealTablePage from      '../components/workflow/applySealTable.vue'
import ApplySealViewPage from       '../components/sealApplyView.vue'

import ApplyRecruitPage from        '../components/workflow/applyRecruit.vue'
import ApplyRecruitTablePage from   '../components/workflow/applyRecruitTable.vue'
import ApplyRecruitViewPage from    '../components/recruitApplyView.vue'

//import ApplyEmployPage from        '../components/workflow/applyEmploy.vue'
// import ApplyEmployTablePage from   '../components/workflow/applyEmployTable.vue'
import ApplyEmployViewPage from    '../components/employApplyView.vue'

import ApplyCarPage from        '../components/workflow/applyCar.vue'
import ApplyCarTablePage from   '../components/workflow/applyCarTable.vue'
import ApplyCarViewPage from    '../components/carApplyView.vue'

import ApplyTeachingCarPage from      '../components/workflow/applyTeachingCar.vue'
import ApplyTeachingCarTablePage from '../components/workflow/applyTeachingCarTable.vue'
import TeachingCarApplyViewPage from  '../components/teachingCarApplyView.vue'

import ApplyEvectionPage from      '../components/workflow/applyEvection.vue'
import ApplyEvectionTablePage from '../components/workflow/applyEvectionTable.vue'
import EvectionApplyViewPage from  '../components/evectionApplyView.vue'

import ApplyMeetingPage from      '../components/workflow/applyMeeting.vue'
import ApplyMeetingTablePage from '../components/workflow/applyMeetingTable.vue'
// import EvectionApplyViewPage from  '../components/evectionApplyView.vue'

import ApplyMeetingRoomPage from      '../components/workflow/applyMeetingRoom.vue'
import ApplyMeetingRoomTablePage from '../components/workflow/applyMeetingRoomTable.vue'
import MeetingRoomApplyViewPage from  '../components/meetingRoomApplyView.vue'


export default [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage
  },
  {
    path: 'panel-left/',
    component: PanelLeftPage
  },
  {
    path: '/panel-right/',
    component: PanelRightPage
  },
  {
    path: '/about/',
    component: AboutPage
  },
  {
    path: '/oa/Task',
    component: TaskPage
  },

  {
    path: '/oa/personalwork/admit/redheadapply/:entityId/:id',
    component: AdmitRedHead
  },
  {
    path: '/oa/personalwork/admit/evectionapply/:entityId/:id',
    component: AdmitEvection
  },
  {
    path: '/oa/personalwork/admit/teachingcarapply/:entityId/:id',
    component: AdmitTeachingCar
  },
  {
    path: '/oa/personalwork/admit/carapply/:entityId/:id',
    component: AdmitCar
  },
  {
    path: '/oa/personalwork/admit/recruitapply/:entityId/:id',
    component: AdmitRecruit
  },
  {
    path: '/oa/personalwork/admit/sealapply/:entityId/:id',
    component: AdmitSeal
  },

  {
    path: '/oa/personalwork/signature',
    component: AdmitSig
  },

  {
    path: '/oa/Myprocess',
    component: MyProcessPage
  },
  {
    path: '/oa/MyInvolvedProcess',
    component: MyInvolvedProcessPage
  },
  {
    path: '/oa/redheadapply/:id',
    component: RedHeadApplyViewPage
  },
  {
    path: '/oa/activities/:id',
    component: ActivitiesViewPage
  },
  {
    path: '/oa/attachments/:id',
    component: AttachmentsViewPage
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage
  },
  {
    path: '/oa/workflow/applyRedHead',
    component: ApplyRedHeadPage
  },
  {
    path: '/oa/workflow/applyRedHeadTable/:id',
    component: ApplyRedHeadTablePage
  },

  {
    path: '/oa/workflow/applySeal',
    component: ApplySealPage
  },
  {
    path: '/oa/workflow/applySealTable/:id',
    component: ApplySealTablePage
  },
  {
    path: '/oa/applySealView/:id',
    component: ApplySealViewPage
  },

  {
    path: '/oa/workflow/applyRecruit',
    component: ApplyRecruitPage
  },
  {
    path: '/oa/workflow/applyRecruitTable/:id',
    component: ApplyRecruitTablePage
  },
  {
    path: '/oa/applyRecruitView/:id',
    component: ApplyRecruitViewPage
  },

  {
    path: '/oa/workflow/additionList/:id',
    component: AdditionListPage
  },

  // {
  //   path: '/oa/workflow/applyEmploy',
  //   component: ApplyEmployPage
  // },
  // {
  //   path: '/oa/workflow/applyEmployTable/:id',
  //   component: ApplyEmployTablePage
  // },
  // {
  //   path: '/oa/applyEmployView/:id',
  //   component: ApplyEmployViewPage
  // },

  {
    path: '/oa/workflow/applyCar',
    component: ApplyCarPage
  },
  {
    path: '/oa/workflow/applyCarTable/:id',
    component: ApplyCarTablePage
  },
  {
    path: '/oa/applyCarView/:id',
    component: ApplyCarViewPage
  },

  {
    path: '/oa/workflow/applyTeachingCar',
    component: ApplyTeachingCarPage
  },
  {
    path: '/oa/workflow/applyTeachingCarTable/:id',
    component: ApplyTeachingCarTablePage
  },
  {
    path: '/oa/applyTeachingCarView/:id',
    component: TeachingCarApplyViewPage
  },

  {
    path: '/oa/workflow/applyEvection',
    component: ApplyEvectionPage
  },
  {
    path: '/oa/workflow/applyEvectionTable/:id',
    component: ApplyEvectionTablePage
  },
  {
    path: '/oa/applyEvectionView/:id',
    component: EvectionApplyViewPage
  },

  {
    path: '/oa/workflow/applyMeeting',
    component: ApplyMeetingPage
  },
  {
    path: '/oa/workflow/applyMeetingTable/:id',
    component: ApplyMeetingTablePage
  },
  // {
  //   path: '/oa/applyEvectionView/:id',
  //   component: EvectionApplyViewPage
  // },

  // {
  //   path: '/oa/workflow/applyMeetingRoom',
  //   component: ApplyMeetingPage
  // },
  // {
  //   path: '/oa/workflow/applyMeetingRoomTable/:id',
  //   component: ApplyMeetingTablePage
  // },
  // {
  //   path: '/oa/applyEvectionView/:id',
  //   component: EvectionApplyViewPage
  // },

  {
    path: '(.*)',
    component: NotFoundPage
  }
]
