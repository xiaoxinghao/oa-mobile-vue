// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

// Import F7
import Framework7 from 'framework7/framework7.esm.bundle.js'

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js'

import global_ from "./components/global.vue"

// Import F7 Styles
import Framework7Styles from 'framework7/css/framework7.css'

import IconsStyles from './css/icons.css'
import AppStyles from './css/app.css'

Vue.config.productionTip = false
Vue.prototype.GLOBAL = global_

Framework7.use(Framework7Vue)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
})
